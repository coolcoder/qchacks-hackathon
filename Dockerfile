FROM jupyter/scipy-notebook
# install the locales you want to use
RUN set -ex \
   && sed -i 's/^# en_US.UTF-8 UTF-8$/en_US.UTF-8 UTF-8/g' /etc/locale.gen \
   && locale-gen en_US.UTF-8 \
   && update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 \

#Python packages to get q sharp to work
RUN set -ex \
   && conda install --quiet --yes \
   # choose the Python packages you need
   'numpy' \
   'qsharp -c microsoft' \
   'jupyterlab_legos_ui' \
   'azure-quantum' \
   'jupyter_contrib_nbextensions  -c conda-forge' \
   'jinja2' \
   'numpy' \
   'protobuf' \
   'azure-storage-blob'
   'aiohttp' \
   'aiofile' \
   'cython' \
   'pillow' 
 
   && conda clean --all -f -y \
   # install Jupyter Lab extensions you need
   && jupyter labextension install jupyterlab-plotly@4.9.0 --no-build \
   && jupyter lab build -y \
   && jupyter lab clean -y \
   && rm -rf "/home/${NB_USER}/.cache/yarn" \
   && rm -rf "/home/${NB_USER}/.node-gyp"

  docker build --rm -t docker-jupyter-extensible .
  printf "UID=$(id -u)\nGID=$(id -g)\n" > .env
  docker-compose up

#https://docs.microsoft.com/en-us/azure/quantum/how-to-python-qdk-local
#the first test to do is creating a quantumhost.qs & host.py(from the above link)
#then you can determine if you can start jupyterlab and open a Q# kernel